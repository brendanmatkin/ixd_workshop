import { SerialPort } from "serialport";
import { ReadlineParser } from "@serialport/parser-readline";
import inquirer from "inquirer";

SerialPort.list().then((ports) => {
  let portChoice = [];
  ports.forEach((port, i) => {
    portChoice.push({
      name: `${port.path} - ${port.friendlyName}`,
      value: port.path,
    });
  });
  inquirer
    .prompt([
      {
        type: "list",
        name: "path",
        message: "Choose port:",
        choices: [...portChoice],
      },
      {
        type: "list",
        name: "baud",
        message: "Choose baud rate:",
        choices: [115200, 9600, 38400, 2000000],
        default: 115200,
      },
    ])
    .then((answers) => {
      connectSerial(answers.path, answers.baud);
    })
    .catch((error) => {
      if (error.isTtyError) {
        console.error("[inquirer] environment not supported");
      } else {
        console.error("[inquirer] error: ", error.message);
      }
    });
});

function connectSerial(serialPath, serialBaud) {
  // Create a port
  const port = new SerialPort({
    path: serialPath,
    baudRate: serialBaud,
  });

  port.on("open", () => {
    port.write("Hello client");
    console.log("Connected to: ", port.path);
    startSerialControl(port);
  });

  port.on("close", () => {
    console.log("port closed.");
  });

  port.on("error", (err) => {
    console.error("PORT ERROR: ", err.message);
  });

  port.on("readable", () => {
    console.log("Data:", port.read().toString());
  });
  const parser = port.pipe(new ReadlineParser({ delimiter: "\r\n" }));
  parser.on("data", console.log);
}

async function inquirerNumber() {
  const answers = await inquirer
    .prompt([
      {
        type: "input",
        name: "ledDelay",
        message: "Choose LED step delay (1 to 1000ms):",
        validate: (value) =>
          value < 1 || value > 1000 || isNaN(value)
            ? `Please enter a number between 1 and 1000`
            : true,
        filter: (value) =>
          value < 1 || value > 1000 || isNaN(value) ? "" : value,
        default: 10,
      },
    ])
    .catch((err) => console.error(err));
  return answers.ledDelay;
}

let timer;
async function startSerialControl(port) {
  let ledDelay = await inquirerNumber();
  // console.log(ledDelay);
  let val = 0;
  clearInterval(timer);
  timer = setInterval(() => {
    port.write(Buffer.from([val]));
    val = val < 255 ? (val += 1) : 0;
  }, ledDelay);
  await startSerialControl(port);
}
