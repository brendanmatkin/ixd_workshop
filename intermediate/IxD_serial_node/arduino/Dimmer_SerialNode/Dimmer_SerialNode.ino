const int ledPin = D4;      // the pin that the LED is attached to

void setup() {
  // initialize the serial communication:
  Serial.begin(115200);
  while(!Serial){};
  delay(1000);
  // initialize the ledPin as an output:
  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, LOW);
  while(!Serial.available()){};
  digitalWrite(ledPin, HIGH);
  Serial.println("Hello");
}

void loop() {
  byte brightness;

  // check if data has been sent from the computer:
  if (Serial.available()) {
    // read the most recent byte (which will be from 0 to 255):
    brightness = Serial.read();
    // set the brightness of the LED:
    analogWrite(ledPin, brightness);
  }
}