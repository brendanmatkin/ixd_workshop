import { Serial, SerialEvents } from "./lib/serial.js";

// Setup Web Serial using serial.js
const serial = new Serial();
serial.on(SerialEvents.CONNECTION_OPENED, onSerialConnectionOpened);
serial.on(SerialEvents.CONNECTION_CLOSED, onSerialConnectionClosed);
serial.on(SerialEvents.DATA_RECEIVED, onSerialDataReceived);
serial.on(SerialEvents.ERROR_OCCURRED, onSerialErrorOccurred);

function onSerialErrorOccurred(eventSender, error) {
  console.log("onSerialErrorOccurred", error);
}

function onSerialConnectionOpened(eventSender) {
  console.log("onSerialConnectionOpened", eventSender);
  document.querySelector("#connect-button").style.display = "none";
  document.querySelector("#interactive-controls").style.display = "block";
}

function onSerialConnectionClosed(eventSender) {
  console.log("onSerialConnectionClosed", eventSender);
}

function onSerialDataReceived(eventSender, newData) {
  console.log("onSerialDataReceived", newData);
  document.querySelector("#rcvd").textContent = newData;
}

async function onConnectButtonClick() {
  console.log("Connect button clicked!");

  if (navigator.serial) {
    if (!serial.isOpen()) {
      await serial.connectAndOpen();
    } else {
      console.log("The serial connection appears already open");
    }
  } else {
    alert("The Web Serial API does not appear supported on this web browser.");
  }
}

async function onSliderValueChanged(event) {
  const src = event.target;
  console.log("Writing to serial: ", src.value.toString());
  serial.writeLine(src.value);

  // update slider value text
  document.querySelector("#slider-value").textContent = src.value;
}

// Initialize values and event listeners

// Get current slider value and set it to the slider text output
const sliderElement = document.querySelector("#slider");
let sliderVal = sliderElement.value;
document.querySelector("#slider-value").textContent = sliderVal;
sliderElement.addEventListener("change", onSliderValueChanged);

document
  .querySelector("#connect-button")
  .addEventListener("click", onConnectButtonClick);
