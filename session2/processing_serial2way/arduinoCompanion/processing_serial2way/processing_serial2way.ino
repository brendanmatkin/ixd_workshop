#include <elapsedMillis.h>

#define BLUE_LED_PIN LED_BUILTIN
#define RED_LED_PIN 0 //esp8266 feather huzzah led is pin 0, HIGH is off

elapsedMillis sendTimer;

uint8_t redLedVal = 0;
uint8_t blueLedVal = 0;


void setup() {
  Serial.begin(115200);
  randomSeed(analogRead(A0));

  pinMode(RED_LED_PIN, OUTPUT);
  analogWrite(RED_LED_PIN, 255-redLedVal);
  pinMode(BLUE_LED_PIN, OUTPUT);
  digitalWrite(BLUE_LED_PIN, 1-blueLedVal);
  
  analogWriteResolution(8);
  sendTimer = 0;
}



void loop() {
  if (sendTimer > 200) {
    int randomInt1 = random(0,100);
    int randomInt2 = random(100, 200);
    Serial.printf("%d,%d\n", randomInt1, randomInt2);
    sendTimer = 0;
  }

  analogWrite(RED_LED_PIN, 255-redLedVal);
  digitalWrite(BLUE_LED_PIN, 1-blueLedVal);
  while (Serial.available()) {  // use this for multi-character or if code has delays
//  if (Serial.available()) {
    char inChar = Serial.read();
    if (inChar==255) {
      Serial.printf("%d,%d\n", 0, 0);
      digitalWrite(RED_LED_PIN, LOW);
    }
    else if (inChar == 0) {
      //
    }
    else if (inChar >0 && inChar < 255) {
      Serial.printf("%d,%d\n", inChar, inChar);
      analogWrite(RED_LED_PIN, 255-inChar); // pin is connected to LED ground, so invert value
    }
  }
}
