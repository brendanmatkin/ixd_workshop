String serialList[];
int baudRate = 115200;


void setupSerial(String portname) {
  if (DEBUG) printArray(Serial.list());    // for TESTING only
  
  serial = new Serial(this, portname, baudRate);
  serialConnected = true;
  
  serialSendTimer = millis();
}

//printArray(Serial.list());

//// Open the port you are using at the rate you want:
//myPort = new Serial(this, Serial.list()[0], 9600);
