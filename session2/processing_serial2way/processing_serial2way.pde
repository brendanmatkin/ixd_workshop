
import processing.serial.*;  // https://processing.org/reference/libraries/serial/index.html
Serial serial;
import java.util.*;

boolean DEBUG = true;

/* Serial variables: */
long serialSendTimer;
boolean serialReceive = true;     // do you want to receive serial? 
boolean serialConnected = false;   // are you connected to serial? 
String receiveBuffer = null;
int[] serialValues = new int[2];

float averageValue1, averageValue2;            // position value sent back from micro.. 

color colorBg = color(255);
color colorTxt = color(10);

color fill1 = color(127, 255, 127);  //255, 127, 127    255, 0, 255, 127
color fill2 = color(255, 127, 127);  //127, 255, 127    0, 255, 255, 127
color stroke1 = color(255, 127);
color stroke2 = color(127);



void setup() {
  size(650, 650);  
  //printArray(PFont.list());
  PFont pFont = createFont("Inconsolata", 13, true);
  textFont(pFont);
  
  setupSerial("COM10");
  
}

void draw() {
  background(colorBg);
  fill(colorTxt);
  text(">>> ", 10, 35);
  text(nf(serialValues[0], 3) + "     " + nf(serialValues[1],3), 40, 35);
  text(">>> ", 10, 55);
  text(nf(averageValue1, 3, 3) + " " + nf(averageValue2,3,3), 40, 55);
  
  if (mousePressed) {
    if (mouseButton == RIGHT) {
      int val = (int)map(mouseX, 0, width, 0, 255);
      serial.write(val);
    }
    else {
      serial.write(255);
    }
  }
  
  /* EWMA Averaging */
  float coef = 0.1;
  averageValue1 = coef*serialValues[0] + (1.0 - coef)*averageValue1;
  averageValue2 = coef*serialValues[1] + (1.0 - coef)*averageValue2;
  
  /* receive from serial: */
  while (serial.available() > 0) {
    int lf = 10;  // line feed character
    receiveBuffer = serial.readStringUntil(lf);
    if (receiveBuffer != null) {
      receiveBuffer = trim(receiveBuffer);
      String[] inStrings = split(receiveBuffer, ',');
      if (inStrings.length == 2) {
        serialValues[0] = int(inStrings[0]);
        serialValues[1] = int(inStrings[1]);
        if (DEBUG) print(">>> " + receiveBuffer);
        if (DEBUG) println("\t-> " + serialValues[0] + "," + serialValues[1]);
      }
    }
    receiveBuffer = null;
  }
  
  //blendMode(DIFFERENCE);
  strokeWeight(2);
  
  stroke(stroke2);
  fill(fill1);
  
  //ellipse(width/2, height/2, serialValues[0], serialValues[0]);
  //ellipse(width/2, height/2, averageValue1, averageValue1);
  //ellipse(width/3, height/2, serialValues[0], serialValues[0]);
  ellipse(width/3, height/2, averageValue1, averageValue1);
  
  stroke(stroke2);
  fill(fill2);
  
  //ellipse(width/2, height/2, serialValues[1], serialValues[1]);
  //ellipse(width/2, height/2, averageValue2, averageValue2);
  //ellipse(width/3*2, height/2, serialValues[1], serialValues[1]);
  ellipse(width/3*2, height/2, averageValue2, averageValue2);
}
