import processing.serial.*;

Serial serial;

String inString;      // dollar sign
int buttonDelim = 44; // splitter (,)
int endDelim = 36;    // end ($)


void setup() {
  size(500, 500);
  printArray(Serial.list());
  // Serial.list()[x] where x is the index that matches your serial port
  serial = new Serial(this, Serial.list()[4], 9600);
  serial.clear();
  inString = serial.readStringUntil(endDelim);
  inString = null;
}

int inVal;
int inBtn;
void draw() {
  background(255,0,inVal);
  fill(255,255,0);
  ellipse(width/2, height/2, inVal*2, inVal*2);
  
  
  while (serial.available() > 0) {
    inString = serial.readStringUntil(endDelim);
    //println(inString);
    if (inString != null) {
      String[] values = splitTokens(inString, " ,$");
      printArray(values);
      if (values.length == 2) {
        inVal = int(values[1]);
        inBtn = int(values[0]);
      }
    }
  }
}
