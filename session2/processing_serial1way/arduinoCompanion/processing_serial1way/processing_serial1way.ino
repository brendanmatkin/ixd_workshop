

const int buttonPin = D4; // D4 for Wemos Board, regular number for Uno
const int potPin = A0;

void setup() {
  Serial.begin(9600);
  pinMode(buttonPin, INPUT_PULLUP);
  while(!Serial){
    // wait until serial is connected
  };
//  Serial.println("Connected");
}


void loop() {
  bool buttonPressed = !digitalRead(buttonPin);
  if (buttonPressed) {
    Serial.print(1);  // write sends a single byte, print sends human readable ascii characters
  }
  else {
    Serial.print(0);
  }
  Serial.print(",");
  uint8_t analogValue = map(analogRead(potPin), 0, 1023, 0, 255);
  Serial.print(analogValue);
  Serial.print("$");
  delay(100);
}
