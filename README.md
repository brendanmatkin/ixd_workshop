# IxD Class Workshop

This repo contains the files for the interaction design class workshop (April 2022).

## Getting Started

Clone or unzip this repo and open it!

### IXD_WEBSERIAL

This project will get us communicating between an Arduino and a browser (in this case it MUST be Chrome or Edge - Firefox doesn't yet support WebSerial). You will need some kind of web server to see any output.

For absolute ease I recommend using **_VSCode_**. If you plan to do some p5js work then install the **_p5.vscode extension_**. Otherwise just the **_Live Server extension_** (it is included with p5.vscode). There are lots of other ways (e.g. [Vite](https://vitejs.dev/) and a [p5 template](https://github.com/makinteract/p5js-vite)) but Live Server is probably the easiest!

Live Server requires **_Node_** - as does most javascript development these days - so you may as well install it now: [NodeJS Download](https://nodejs.org/en/download/).

### IXD_SERIAL_NODE

This is a Node JS project using the serialport library and some command line options. It's a pinch more advanced. Helpful if you want to run node on a pi or have a server that spits out some websockets or something.

Make sure you have the latest Node LTS installed (16.14.2 at the time of writing). Open the folder in a terminal and `npm install`, then `npm start`. Follow the prompts! Works with arduino `communication/dimmer` example (or `./arduino/Dimmer_SerialNode/`)

### PD

Some bare-bones Pure Data examples to communicate with the Arduino IDE "communication" examples. Use the [purr-data](https://github.com/agraef/purr-data/releases) variant of PD.

## Notion

There is a notion page with my notes on it here: [Brendan's Notion - IxD Workshop](https://brendanmatkin.notion.site/IxD-Class-Arduino-Communication-Workshop-13e910236d7e48cca6fa14054a3ea29e).

## Credit

Some of these projects were taken from [Makeability Lab Physical Computing Learning Space](https://makeabilitylab.github.io/physcomp/) and modified. I strongly recommend taking a look at this fantastic learning resource!
